// memorymanager.h
// Data structure to keep track of free and used pages.
// The only way to create one is by calling the following:
//      MemoryManager->GetInstance();
// We make the first call of this sort in StartProcess in progtest.cc. This will initialize
// the static global memManager variable for the first time. Once it has been initialized, it
// will continue to return the same instance of the memoryManager each time.


#ifndef MEMORYMANAGER_H
#define MEMORYMANAGER_H

#include "bitmap.h"
#include "synch.h"

class MemoryManager
{
public:
    static MemoryManager* GetInstance();
    ~MemoryManager();
    int AllocPage(); // Allocate a free page, returning its physical page number or -1.
    void FreePage(int physPageNum); //Free the physical page and make it available for future allocation.
    bool PageIsAllocated(int physPageNum); //True if the physical page is allocated, false otherwise.
    
private:
    MemoryManager(int numPages);
    static MemoryManager* memManager;
    BitMap* pages;
    Lock* addrLock;
};

#endif
