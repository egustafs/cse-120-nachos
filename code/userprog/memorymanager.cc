#include "memorymanager.h"
#include "machine.h"

MemoryManager* MemoryManager::memManager = NULL;

MemoryManager*
MemoryManager::GetInstance()
{
    if (memManager == NULL)
        memManager = new MemoryManager(NumPhysPages);
    
    return memManager;
}

MemoryManager::MemoryManager(int numPages)
{
    pages = new BitMap(numPages);
    addrLock = new Lock("addrLock");
}

MemoryManager::~MemoryManager()
{
    delete pages;
    delete addrLock;
}

// Allocate a free page, returning its physical page number or -1
// Keep in mind that pages->Find() will already Mark the free
// page found as used.
int
MemoryManager::AllocPage()
{
    addrLock->Acquire();
    int pageNumber = pages->Find();
    addrLock->Release();
    
    return pageNumber;
}

// Free the physical page and make it available for future allocation
void
MemoryManager::FreePage(int physPageNum)
{
    addrLock->Acquire();
    if (PageIsAllocated(physPageNum))
        pages->Clear(physPageNum);
    addrLock->Release();
}

// True if the physical page is allocated, false otherwise.
bool
MemoryManager::PageIsAllocated(int physPageNum)
{
    addrLock->Acquire();
    bool result = pages->Test(physPageNum);
    addrLock->Release();
    
    return result;
}
