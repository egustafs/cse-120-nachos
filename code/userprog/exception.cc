// exception.cc
//  Entry point into the Nachos kernel from user programs.
//  There are two kinds of things that can cause control to
//  transfer back to here from user code:
//
//  syscall -- The user code explicitly requests to call a procedure
//  in the Nachos kernel.  Right now, the only function we support is
//  "Halt".
//
//  exceptions -- The user code does something that the CPU can't handle.
//  For instance, accessing memory that doesn't exist, arithmetic errors,
//  etc.
//
//  Interrupts (which can also cause control to transfer from user
//  code into the Nachos kernel) are handled elsewhere.
//
// For now, this only handles the Halt() system call.
// Everything else core dumps.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "syscall.h"
#include "filesys.h"
#include <string>
#include <vector>
#include <iterator>
//----------------------------------------------------------------------
// ExceptionHandler
//  Entry point into the Nachos kernel.  Called when a user program
//  is executing, and either does a syscall, or generates an addressing
//  or arithmetic exception.
//
//  For system calls, the following is the calling convention:
//
//  system call code -- r2
//      arg1 -- r4
//      arg2 -- r5
//      arg3 -- r6
//      arg4 -- r7
//
//  The result of the system call, if any, must be put back into r2.
//
// And don't forget to increment the pc before returning. (Or else you'll
// loop making the same system call forever!
//
//  "which" is the kind of exception.  The list of possible exceptions
//  are in machine.h.
//----------------------------------------------------------------------
void ExecExceptionCall();
void exit(int status);

const int MAX_LENGTH = 255;

void
SystemCall(int type)
{
    switch (type)
    {
        case SC_Halt:
        {
            DEBUG('a', "Shutdown, initiated by user program.\n");
            interrupt->Halt();
            break;
        }
        case SC_Exec:
        {
            ExecExceptionCall();
            break;
        }
        case SC_Exit:
        {
            //exit(0);

        }
        break;

       
    }
}

void
ExceptionHandler(ExceptionType which)
{
    int type = machine->ReadRegister(2);
    
    switch (which)
    {
        case SyscallException:
        {
            SystemCall(type);
            break;
        }

        case NoException:
        {
            printf("Everything OK!\n");
            break;
        }

        case PageFaultException:
        {
            printf("PageFaultException\n");
            exit(1);
            break;
        }
        case ReadOnlyException:
        {
            printf("ReadOnlyException\n");
            exit(1);
            break;
        }
        case BusErrorException:
        {
            printf("BusErrorException\n");
            exit(1);
            break;
        }
        case AddressErrorException:
        {
            printf("AddressErrorException\n");
            exit(1);
            break;
        }
        case OverflowException:
        {
            printf("OverflowException\n");
            exit(1);
            break;
        }
        case IllegalInstrException:
        {
            printf("IllegalInstrException\n");
            exit(1);
            break;
        }
        case NumExceptionTypes:
        {
            printf("NumExceptionTypes\n");
            exit(1);
            break;
        }

        default :
        {
            printf("Unexpected user mode exception %d %d\n", which, type);
            ASSERT(FALSE);
            break;
        }
    }
}



SpaceId Exec(char *name, int argc, char **argv, int opt) {
    // arguments *args = {
    //     opt,
    //     name,
    // };
    //need to do name string checks here
    SpaceId spaceId = 0;

    OpenFile *executable = fileSystem->Open(name);
    AddrSpace *space;

    if (executable == NULL) {
        printf("Unable to open file %s\n", name);
        return -1;
    }
    Thread* thread = new Thread("child", 0);
    space = new AddrSpace(executable);

    delete executable;          // close file

    thread->space = space;

    thread->Fork((VoidFunctionPtr)RunExecSyscall, 0);

    return spaceId;
    
}

void 
ExecExceptionCall()
{
    int currChar;
    char path[MAX_LENGTH];
    int length = 0;

    //machine->ReadRegister(4) is the int nameVirtualAddress
    int vaddr = machine->ReadRegister(4);

    do{
        machine->ReadMem(vaddr, 1, &currChar);

        path[length] = *(char*)&currChar;
        length ++;
        
    }while(*(char*)&currChar != '\0' || length <= MAX_LENGTH);


    if(*(char*)&currChar == '\0')
    {
        // pathPtr = new char[path.size()];
        // std::copy(path.begin(), path.end(), pathPtr);

        Exec(path, 0, NULL, 0);
    }
    else
    {
        // NO ENDLINE OR PATH LONGER THAN MAX_LENGTH
    }
    // don't forget to delete[] pathPtr at somepoint;
}

void RunExecSyscall(){

    currentThread->space->InitRegisters();     // set the initial register values
    currentThread->space->RestoreState();      // load page table register

    machine->Run();         // jump to the user progam
    ASSERT(FALSE);          // machine->Run never returns;
    // the address space exits
    // by doing the syscall "exit"

    /*
    cases:
    filename string crosses user page boundaries and resides in noncontiguous
      physical memory
    detect illegal string address or a string that runs off the end of the
      user's address space without a terminating null character
    todo:
    returning an error (SpaceId 0) from the Exec system call
    */
}

void exit(int status){

    DEBUG('a', "status %d \n", status);
    DEBUG('a', "thread %s exit\n", currentThread->getName());
     
    Table::GetInstance()->Release(Table::GetInstance()->GetIndexofSpaceId(currentThread->space->GetID())); //release space in process
    delete currentThread->space;
    currentThread->Finish();
    //not sure
}





Table* Table::table = NULL;

/* Get instance of table and create if necessary*/
Table* Table::GetInstance()
{
    if (table == NULL)
        table = new Table(32);
    
    return table;
}

/* Create a table to hold at most "size" entries. */
Table::Table(int size) {
    tableLock = new Lock("tableLock");
    vec.assign(size, (void *)NULL);
}

Table::~Table() {
    delete tableLock; 
}

/* Help find vector index of the passed in SpaceID, for use in Exit*/
int Table::GetIndexofSpaceId(SpaceId * t_id) {
    tableLock->Acquire();
     
    int retval = -1; 
    for(int i = 0; i < (int)vec.size(); i++) {
        if(vec[i] == (void *)t_id) {
	    retval = i;
	    break;
	}
    }

    tableLock->Release();
     
    return retval;
}

/* Allocate a table slot for "object", returning the "index" of the
   allocated entry; otherwise, return -1 if no free slots are available. */
int Table::Alloc(void * object) {
    tableLock->Acquire();

    int retval = -1;
    for(int i = 0; i < (int)vec.size(); i++) {
        if(vec[i] == NULL) {
	    vec[i] = object;
	    retval = i;
	    break;
	}
    }
    tableLock->Release();

    return retval;
}

/* Retrieve the object from table slot at "index", or NULL if that
   slot has not been allocated. */
void * Table::Get(int index) {
    tableLock->Acquire();
    void * retval = NULL;

    if(index >= 0 && index <= (int)vec.size()-1 && vec[index] != NULL)
    	retval = vec[index];
    
    tableLock->Release();
    return retval;
}

/* Free the table slot at index. */
void Table::Release(int index) {
    tableLock->Acquire();

    if(index >= 0 && index < (int)vec.size()-1)
        vec[index] = NULL;
    
    tableLock->Release();
}
