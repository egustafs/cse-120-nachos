// synch.cc
//	Routines for synchronizing threads.  Three kinds of
//	synchronization routines are defined here: semaphores, locks
//   	and condition variables (the implementation of the last two
//	are left to the reader).
//
// Any implementation of a synchronization routine needs some
// primitive atomic operation.  We assume Nachos is running on
// a uniprocessor, and thus atomicity can be provided by
// turning off interrupts.  While interrupts are disabled, no
// context switch can occur, and thus the current thread is guaranteed
// to hold the CPU throughout, until interrupts are reenabled.
//
// Because some of these routines might be called with interrupts
// already disabled (Semaphore::V for one), instead of turning
// on interrupts at the end of the atomic operation, we always simply
// re-set the interrupt state back to its original value (whether
// that be disabled or enabled).
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "synch.h"
#include "system.h"

//----------------------------------------------------------------------
// Semaphore::Semaphore
// 	Initialize a semaphore, so that it can be used for synchronization.
//
//	"debugName" is an arbitrary name, useful for debugging.
//	"initialValue" is the initial value of the semaphore.
//----------------------------------------------------------------------

Semaphore::Semaphore(char* debugName, int initialValue)
{
    name = debugName;
    value = initialValue;
    queue = new List;
}

//----------------------------------------------------------------------
// Semaphore::Semaphore
// 	De-allocate semaphore, when no longer needed.  Assume no one
//	is still waiting on the semaphore!
//----------------------------------------------------------------------

Semaphore::~Semaphore()
{
    delete queue;
}

//----------------------------------------------------------------------
// Semaphore::P
// 	Wait until semaphore value > 0, then decrement.  Checking the
//	value and decrementing must be done atomically, so we
//	need to disable interrupts before checking the value.
//
//	Note that Thread::Sleep assumes that interrupts are disabled
//	when it is called.
//----------------------------------------------------------------------

void
Semaphore::P()
{
    IntStatus oldLevel = interrupt->SetLevel(IntOff);	// disable interrupts

    while (value == 0) { 			// semaphore not available
        queue->SortedInsert((void *)currentThread, currentThread->getPriority() * -1);	// so go to sleep
        currentThread->Sleep();
    }
    value--; 					// semaphore available,
    // consume its value

    (void) interrupt->SetLevel(oldLevel);	// re-enable interrupts
}

//----------------------------------------------------------------------
// Semaphore::V
// 	Increment semaphore value, waking up a waiter if necessary.
//	As with P(), this operation must be atomic, so we need to disable
//	interrupts.  Scheduler::ReadyToRun() assumes that threads
//	are disabled when it is called.
//----------------------------------------------------------------------

void
Semaphore::V()
{
    Thread *thread;
    IntStatus oldLevel = interrupt->SetLevel(IntOff);

    thread = (Thread *)queue->Remove();
    if (thread != NULL)	   // make thread ready, consuming the V immediately
        scheduler->ReadyToRun(thread);
    value++;
    (void) interrupt->SetLevel(oldLevel);
}

// Dummy functions -- so we can compile our later assignments
// Note -- without a correct implementation of Condition::Wait(),
// the test case in the network assignment won't work!
Lock::Lock(char* debugName) {

    queue = new List;
    value = 0; 
    name = debugName;
    
}
Lock::~Lock() {

    delete queue;

}

void Lock::Acquire() {

    IntStatus oldLevel = interrupt->SetLevel(IntOff);   // disable interrupts
    ASSERT (!isHeldByCurrentThread());  //currentThread should not try to re-aquire the same lock.
    
    //Priority Inversion Check
    //masterThread points to the thread currently holding the lock so make sure
    //a thread is holding the lock.
    if (masterThread != NULL) {
        int heldThreadPriority = masterThread->getPriority();
        int currentThreadPriority = currentThread->getPriority();
        
        DEBUG ('t', "There is another thread, %s currently holding a lock with priority %d. \n", masterThread->getName(), heldThreadPriority);
        DEBUG ('t', "The current thread %s has a priority of %d. \n", currentThread->getName(), currentThreadPriority);
        
        // if the thread needing the lock (currentThread), has a priority higher
        // than the thread holding the lock (masterThread), then we donate the
        // higher priority to the thread holding the lock so that it can finish
        // faster and not block cpu to the higher priority thread needing the lock.
        if (currentThreadPriority > heldThreadPriority) {
            originalPriority = heldThreadPriority;
            masterThread->setPriority(currentThreadPriority); //this set priority will trigger a scheduler priority update.
            
            DEBUG ('t', "OriginalPriority is now %d \n", originalPriority);
            DEBUG ('t', "The lock holding thread, %s now has a priority of %d. \n", masterThread->getName(), masterThread->getPriority());
        }

    }
    
    while (value) {
        queue->SortedInsert((void*)currentThread, currentThread->getPriority() * -1);
        currentThread->Sleep(); //Sleep!
    } 
    value = true;       // semaphore available, 
                        // consume
    
    originalPriority = currentThread->getPriority();
    masterThread = currentThread;   //a new thread has acquired the lock so update it to be the thread holding the lock.
    DEBUG ('t', "%s has acquired the lock.\n", masterThread->getName());
    (void) interrupt->SetLevel(oldLevel); // re-enable interrupts
}
void Lock::Release() {

    Thread *thread;
    
    ASSERT (isHeldByCurrentThread());
    ASSERT (value);

    if (isHeldByCurrentThread())
    {
        IntStatus oldLevel = interrupt->SetLevel(IntOff);

        thread = (Thread *)queue->Remove();
        if (thread != NULL)    //Thread ready to comsume V
            scheduler->ReadyToRun(thread);

        (void) interrupt->SetLevel(oldLevel); // re-enable interrupts
    }

    DEBUG ('t', "%s has released the lock.", currentThread->getName());
    
    value = false;
    masterThread = NULL;
    DEBUG ('t', "originalPriority is now %d", originalPriority);
    
    currentThread->setPriority(originalPriority); //reset the priority of the current thread to it's old priority for priority inversion.
    
    DEBUG ('t', "%s has released the lock, now has priority %d \n", currentThread->getName(), currentThread->getPriority());}


bool Lock::isHeldByCurrentThread()
{
    return masterThread == currentThread;
}


Condition::Condition(char* debugName) {
    name = debugName;
    queue = new List;
 }

Condition::~Condition() { 
    delete queue;
}

void Condition::Wait(Lock* conditionLock) {

    IntStatus oldLevel = interrupt->SetLevel(IntOff);

    ASSERT (conditionLock->isHeldByCurrentThread());
    
    
    queue->SortedInsert((void *)currentThread, currentThread->getPriority() * -1);
    conditionLock->Release();
    currentThread->Sleep();    
    conditionLock->Acquire();

    (void) interrupt->SetLevel(oldLevel);
}

void Condition::Signal(Lock* conditionLock) { 

    IntStatus oldLevel = interrupt->SetLevel(IntOff);
 
    Thread *thread;
    
    ASSERT (conditionLock->isHeldByCurrentThread());

    thread = (Thread *)queue->Remove();

    if (NULL != thread)
        scheduler->ReadyToRun(thread);
    
    (void) interrupt->SetLevel(oldLevel);
    
}

void Condition::Broadcast(Lock* conditionLock) { 

  ASSERT (conditionLock->isHeldByCurrentThread());

  IntStatus oldLevel = interrupt->SetLevel(IntOff);

    while (queue->IsEmpty() == false)
    {
        Signal(conditionLock);
    }

    (void) interrupt->SetLevel(oldLevel);

}


Mailbox::Mailbox(char* debugname) {
    //initializing the variables
    name = debugname;

    lock = new Lock(debugname);
    sent = new Condition(debugname);
    received = new Condition(debugname);

    numSenders = 0;
    numReceivers = 0;
}
Mailbox::~Mailbox() {}
void Mailbox::Send(int message) {
    //acquire lock
    lock->Acquire();

    //spinlock when there are no receivers
    while(numReceivers == 0){
        received->Wait(lock); // wait releases lock & suspend execu of calling thread
    }
    //when out of spinklock, copy message over
    //increment senders
    //signal sent CV
    messg = message;
    numSenders++;
    sent->Signal(lock);

    //release lock
    lock->Release();
}
void Mailbox::Receive(int * message){
    //acquire lock
    lock->Acquire();

    //increment receivers count and spinlock when completed senders aren't avail
    numReceivers++;
    while(numSenders == 0) {
        received->Signal(lock);
        sent->Wait(lock);
    }
    //copy message to param
    //decrement both receiver and sender
    *message = messg;
    numReceivers--;
    numSenders--;
    received->Signal(lock);

    //release lock
    lock->Release();
}

Whale::Whale()
{
    mated = 0;
    // Waiting whale counts
    maleCount = 0;
    femaleCount = 0;
    matchCount =0;

    // Lock for count modification and checks
    countLock = new Lock("countLock");

    // Lock for whale queue manipulation
    whaleLock = new Lock("whaleLock");

    // Condition variables holding waiting whale queues
    maleCond = new Condition("maleCond");
    femaleCond = new Condition("femaleCond");
    matchCond = new Condition("matchCond");        

}

void Whale::Male()
{
    // Acquire lock for whales
    whaleLock->Acquire();

    // Acquire lock for count variables
    countLock->Acquire();

    DEBUG ('w', "\nMale Whale Count: %d\n", maleCount + 1);
    DEBUG ('w', "Female Whale Count: %d\n", femaleCount);
    DEBUG ('w', "Matchmaker Whale Count: %d\n\n", matchCount);

    // If there exists at least one of each return this whale
    //      and wake 1 of each of the other 2 types
    if(maleCount >= 0 && femaleCount > 0 && matchCount > 0)
    {
        femaleCond->Signal(whaleLock);
        femaleCount--;

        matchCond->Signal(whaleLock);
        matchCount--;

        DEBUG ('w', "%d Whales Mated\n", ++mated);

        // Done with count, release lock
        countLock->Release();

    }
    // If there is not at least 1 of each, put this whale to sleep
    else
    {
        // Add one whale to count, release count lock
        maleCount++;

        countLock->Release();
        
        DEBUG ('w', "Male Whale Wait\n");
        // Put whale to sleep
        maleCond->Wait(whaleLock);
    }

    DEBUG ('w', "Male whale gets it on\n");
    // release whale lock
    whaleLock->Release();

}

void Whale::Female()
{
    // Acquire lock for whale    
    whaleLock->Acquire();

    // Acquire lock for count variables    
    countLock->Acquire();

    DEBUG ('w', "\nMale Whale Count: %d\n", maleCount);
    DEBUG ('w', "Female Whale Count: %d\n", femaleCount + 1);
    DEBUG ('w', "Matchmaker Whale Count: %d\n\n", matchCount);

    // If there exists at least one of each return this whale
    //      and wake 1 of each of the other 2 types
    if(maleCount > 0 && femaleCount >= 0 && matchCount > 0)
    {
        maleCond->Signal(whaleLock);
        maleCount--;

        matchCond->Signal(whaleLock);
        matchCount--;


        DEBUG ('w', "%d Whales Mated\n", ++mated);

        // Done with count, release lock        
        countLock->Release();

    }
    // If there is not at least 1 of each, put this whale to sleep    
    else
    {
        // Add one whale to count, release count lock     
        femaleCount++;
        countLock->Release();

        DEBUG ('w', "Female Whale Wait\n");

        // Put whale to sleep
        femaleCond->Wait(whaleLock);
    }

    DEBUG ('w', "Female whale gets it on\n");

    // release whale lock
    whaleLock->Release();
}

void Whale::Matchmaker()
{
    // Acquire lock for whale    
    whaleLock->Acquire();

    // Acquire lock for count variables    
    countLock->Acquire();

    DEBUG ('w', "\nMale Whale Count: %d\n", maleCount);
    DEBUG ('w', "Female Whale Count: %d\n", femaleCount);
    DEBUG ('w', "Matchmaker Whale Count: %d\n\n", matchCount + 1);

    // If there exists at least one of each return this whale
    //      and wake 1 of each of the other 2 types
    if(maleCount > 0 && femaleCount > 0 && matchCount >= 0)
    {
        maleCond->Signal(whaleLock);
        maleCount--;

        femaleCond->Signal(whaleLock);
        femaleCount--;

        DEBUG ('w', "%d Whales Mated\n", ++mated);

        // Done with count, release lock        
        countLock->Release();
    }
    // If there is not at least 1 of each, put this whale to sleep    
    else
    {
        // Add one whale to count, release count lock        
        matchCount++;        
        countLock->Release();

        DEBUG ('w', "Matchmaker Whale Wait\n");       

        // Put whale to sleep
        matchCond->Wait(whaleLock);
    }
    DEBUG ('w', "Matchmaker lends a hand\n");
    // release whale lock
    whaleLock->Release();
}
