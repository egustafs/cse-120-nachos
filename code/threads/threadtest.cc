// threadtest.cc 
//  Test cases for the threads assignment.

#include "copyright.h"
#include "system.h"
#include "synch.h"

// testnum is set in main.cc
int testnum = 1;

//----------------------------------------------------------------------
// SimpleThread
//  Loop 5 times, yielding the CPU to another ready thread 
//  each iteration.
//
//  "which" is simply a number identifying the thread, for debugging
//  purposes.
//----------------------------------------------------------------------

void
SimpleThread(int which)
{
    int num;
    
    for (num = 0; num < 5; num++) {
    printf("*** thread %d looped %d times\n", which, num);
        currentThread->Yield();
    }
}

//----------------------------------------------------------------------
// ThreadTest1
//  Set up a ping-pong between two threads, by forking a thread 
//  to call SimpleThread, and then calling SimpleThread ourselves.
//----------------------------------------------------------------------

void
ThreadTest1()
{
    DEBUG('t', "Entering ThreadTest1");

    Thread *t = new Thread("forked thread");

    t->Fork(SimpleThread, 1);
    SimpleThread(0);
}

//----------------------------------------------------------------------
// LockTest and Condition
//----------------------------------------------------------------------

Lock *locktest1 = NULL;

void
LockThread1(int param)
{
    printf("L1:0\n");
    locktest1->Acquire();
    printf("L1:1\n");
    currentThread->Yield();
    printf("L1:2\n");
    locktest1->Release();
    printf("L1:3\n");
}

void BroadcastThread2(int _choice) { //for waiting
    int i = 1;
    int* choice = (int*) _choice;
    Lock* lock = (Lock*) choice[1];
    lock->Acquire();
    bool* resource_is_ready = (bool*) choice[2];
    *resource_is_ready = i;
    Condition* cond = (Condition*) choice[0];
    cond->Broadcast(lock);
    lock->Release();
}


void
LockThread2(int param)
{
    printf("L2:0\n");
    locktest1->Acquire();
    printf("L2:1\n");
    currentThread->Yield();
    printf("L2:2\n");
    locktest1->Release();
    printf("L2:3\n");
}

void Signalthread(int _choice) {
    int* choice = (int*) _choice;
    int i = 1;
    Lock* lock = (Lock*) choice[i];
    lock->Acquire();
    bool* resource = (bool*) choice[2];
    *resource = i;

    Condition* cond = (Condition*) choice[0];
    cond->Signal(lock);

    lock->Release();
}

void LockThread3(int args) {
    Lock* lock = (Lock*) args;
    ASSERT(!lock->isHeldByCurrentThread());
    printf("It should Crash soon!!!! Not suppose to release a lock not owned by itself!!!\n");
    lock->Release();
}


void
LockTest1()
{
    DEBUG('t', "Entering LockTest1");

    locktest1 = new Lock("LockTest1");

    Thread *t = new Thread("one");
    t->Fork(LockThread1, 0);
    t = new Thread("two");
    t->Fork(LockThread2, 0);
}

void Test_release() {
    Lock* lock = new Lock("mama");
    Thread* t = new Thread("t3");
    lock->Acquire();
    printf("Process mama Acquired\n");  
    t->Fork(LockThread3, (int)lock);
    currentThread->Yield();
}

void signalThread2(int _choice) { //for waiting
    int* choice = (int*) _choice;
    Lock* lock = (Lock*) choice[1];
    lock->Acquire();

    Condition* cond = (Condition*) choice[0];
    ASSERT (cond->q_isEmpty());
    cond->Signal(lock);
    lock->Release();
}

void Test_delete() {
    Lock *lock = new Lock("Cat");
    lock->Acquire();   
    printf("I have a name called %s\n",lock->getName());
    delete lock;    
    if(lock->getName() == NULL){
        printf("yea, deleted!\n");
    }
}


void WaitThread(int _choice) {
    int* choice = (int*) _choice;    
    Lock* lock = (Lock*) choice[1];
    lock->Acquire();
    bool* resource = (bool*) choice[2];
    Condition* cond = (Condition*) choice[0];
    
    while (!*resource) {
        cond->Wait(lock);
    }

    lock->Release();
}

void broadcastThread(int _choice) {
    int* choice = (int*) _choice;
    int i = 1;
    Condition* cond = (Condition*) choice[0];
    Lock* lock = (Lock*) choice[i];
    lock->Acquire();
    ASSERT (cond->q_isEmpty());

    printf("All wait thread Broadcast\n");
    cond->Broadcast(lock);
    lock->Release();
}

void makeThread(int arg, int _choice){
    int* choice = (int*) _choice;

    Thread* t;
    t = new Thread("First Thread");
    t->Fork(WaitThread, (int)choice);

    Thread* t2;
    t2 = new Thread("2nd Thread");
    t2->Fork(WaitThread, (int)choice);

    if(arg == 0){
        Thread* t3;
        t3 = new Thread("broadcast");
        t3->Fork(BroadcastThread2, (int)choice);
    }
    else{
        Thread* t3;
        t3 = new Thread("signal");
        t3->Fork(Signalthread, (int)choice);
    }
}


void test_cond() { //just cond
    int* choice = new int[3];

    Lock *lock = new Lock("condLock");
    choice[1] = (int)lock;

    Condition *cond = new Condition("cond");
    choice[0] = (int)cond;

    bool *resource = new bool;
    choice[2] = (int)resource;
    *resource = 0;

    makeThread(1, (int)choice);

    currentThread->Yield();
}



void test_cond2(bool arg){ // test waiting
    int* choice = new int[3];
    Thread* t;
    Thread* t2;
    Lock *lock = new Lock("condLock");
    choice[1] = (int)lock;
    Condition *cond = new Condition("cond");
    choice[0] = (int)cond;
    int testchoice = arg; // 1 is signal, 0 is broadcast.
    bool *resource = new bool;
    choice[2] = (int)resource;
    *resource = 0;

    if (!testchoice) {
        t = new Thread("broadcastThread");
        t->Fork(broadcastThread, (int)choice);
    }else{ 
        t2 = new Thread("signalThread");
        t2->Fork(signalThread2, (int)choice);
    }

    currentThread->Yield();

    t = new Thread("wait");
    t->Fork(WaitThread, (int)choice);
    
    currentThread->Yield();

    ASSERT (!cond->q_isEmpty());
    printf ("Done Block!\n");


}

void testcond3() {   //broadcast
    int* choice = new int[3];

    Lock *lock = new Lock("condBroadLock");
    choice[1] = (int)lock;

    Condition *cond = new Condition("condBroad");
    choice[0] = (int)cond;

    bool *resource = new bool;
    choice[2] = (int)resource;
    *resource = 0;

    makeThread(0,(int)choice);

    currentThread->Yield();
}


void
Thread1(int which)
{
    printf("Entering Thread1: %d\n", which);
}

void
Thread2(int which)
{
    printf("Entering Thread2: %d\n", which);
}

void
Thread3(int which)
{
    printf("Entering Thread3: %d\n", which);
}

void
PriorityTest1()
{
    Thread *t1 = new Thread("Thread 1");
    t1->setPriority(1);
    Thread *t2 = new Thread("Thread 2");
    t2->setPriority(2);
    Thread *t3 = new Thread("Thread 3");
    t3->setPriority(3);
    
    t1->Fork(Thread1, 1);
    t2->Fork(Thread2, 2);
    t3->Fork(Thread3, 3);
    
    //Answer should be t3,t2,t1
}

void
PriorityTest2()
{
    Thread *t1 = new Thread("Thread 1");
    t1->setPriority(3);
    Thread *t2 = new Thread("Thread 2");
    t2->setPriority(2);
    Thread *t3 = new Thread("Thread 3");
    t3->setPriority(1);
    
    t1->Fork(Thread1, 1);
    t2->Fork(Thread2, 2);
    t3->Fork(Thread3, 3);
    
    //Answer should be t1,t2,t3
}

void
PriorityTest3()
{
    Thread *t1 = new Thread("Thread 1");
    t1->setPriority(10);
    Thread *t2 = new Thread("Thread 2");
    t2->setPriority(-5);
    Thread *t3 = new Thread("Thread 3");
    t3->setPriority(5);
    
    t1->Fork(Thread1, 1);
    t2->Fork(Thread2, 2);
    t3->Fork(Thread3, 3);
    
    //Answer should be t1,t3,t2
}


void
PriorityTest4() {
    Thread *t1 = new Thread("Thread 1");
    t1->setPriority(10);
    Thread *t2 = new Thread("Thread 2");
    t2->setPriority(5);
    Thread *t3 = new Thread("Thread 3");
    t3->setPriority(5);
    
    t1->Fork(Thread1, 1);
    t2->Fork(Thread2, 2);
    t3->Fork(Thread3, 3);
}

//-----------------
//Priority Inversion test for Locks
//-----------------

void
PiThread3(int which)
{
    printf("L3:0\n");
    locktest1->Acquire();
    printf("L3:1\n");
    //currentThread->Yield();
    //printf("L3:2\n");
    locktest1->Release();
    printf("L3:3\n");
}

void
PiThread2(int which)
{
    printf("L2:0\n");
    Thread *t3 = new Thread("Thread 3");
    t3->setPriority(3);
    t3->Fork(PiThread3, 3);
    
    currentThread->Yield();
}

void
PiThread1(int which)
{
    printf("L1:0\n");
    locktest1->Acquire();
    
    Thread *t2 = new Thread("Thread 2");
    t2->setPriority(2);
    t2->Fork(PiThread2, 2);
    
    currentThread->Yield();
    
    locktest1->Release();
}

void
PriorityInversionTest1()
{
    Thread *t1 = new Thread("Thread 1");
    t1->setPriority(1);
    locktest1 = new Lock("Priority Inversion Lock");
    
    t1->Fork(PiThread1, 1);

}

//-----------------
//Priority Inversion test for Joins
//-----------------

void
PiThreadJoin2(int which)
{
    printf("L2:0\n");
    currentThread->Yield();
    printf("L2:1\n");
}

void
PiThreadJoin1(int which)
{
    printf("L1:0\n");
    printf("L1:1\n");
}

void
PiThreadJoin3(int which)
{
    printf("L3:0\n");
    
    Thread *t1 = new Thread("Thread 1", 1);
    t1->setPriority(1);
    t1->Fork(PiThreadJoin1, 1);
    t1->Join();
    
    Thread *t2 = new Thread("Thread 2", 0);
    t2->setPriority(2);
    t2->Fork(PiThreadJoin2, 2);
    
    printf("L3:1\n");
    
    currentThread->Yield();
    printf("L3:2\n");
}


void
PriorityInversionJoinTest1()
{
    Thread *t3 = new Thread("Thread 3");
    t3->setPriority(3);
    
    t3->Fork(PiThreadJoin3, 3);
}

struct MB_Param {
    Mailbox * mbx;
    int mg;
};

void
Send(MB_Param * mix) {
    //printf("in threadtest send\n");
    //printf("mix.mg -> %d\n", mix->mg);
    (mix->mbx)->Send(mix->mg);
    printf("Sent message: %d\n", mix->mg);
}

void
Receive(Mailbox * mb) {
    int mesg;
    mb->Receive(&mesg);
    printf("Received message: %d\n", mesg);
}

void
MailboxTest()
{
    Mailbox * mb = new Mailbox("mb");
    Thread * t1 = new Thread("Thread 1");
    t1->setPriority(3);
    Thread * t2 = new Thread("Thread 2");
    t2->setPriority(3);
    int message = 1234;
    MB_Param * mix2 = new MB_Param;
    mix2->mbx = mb;
    mix2->mg = message;
    //printf("initialized message: %d\tmailb0x: %s\n", mix2->mg, mix2->mbx->getName());

    t2->Fork((VoidFunctionPtr)Receive, (int)mb);
    t1->Fork((VoidFunctionPtr)Send, (int)mix2);
    //printf("rechecking: %d\tmailb0x: %s\n", mix2->mg, mix2->mbx->getName());
}

void
MaleWhale(Whale * whale)
{
    whale->Male();
}

void
FemaleWhale(Whale * whale)
{
    whale->Female();
}

void
MatchWhale(Whale * whale)
{
    whale->Matchmaker();
}

void
WhaleTest1()
{
    Whale *whale = new Whale();
    Thread *t1 = new Thread("Thread 1");
    Thread *t2 = new Thread("Thread 2");
    Thread *t3 = new Thread("Thread 3");

    DEBUG ('w', "Lets mate some whales\n");

    t1->Fork((VoidFunctionPtr)MaleWhale, (int)whale);
    t2->Fork((VoidFunctionPtr)FemaleWhale, (int)whale);
    t3->Fork((VoidFunctionPtr)MatchWhale, (int)whale);
}

void
WhaleTest2()
{
    Whale *whale = new Whale();
    Thread *t1 = new Thread("Thread 1");
    Thread *t2 = new Thread("Thread 2");
    Thread *t3 = new Thread("Thread 3");
    Thread *t4 = new Thread("Thread 4");
    Thread *t5 = new Thread("Thread 5");

    DEBUG ('w', "Lets mate some whales\n");

    t1->Fork((VoidFunctionPtr)MaleWhale, (int)whale);
    t2->Fork((VoidFunctionPtr)MaleWhale, (int)whale);
    t3->Fork((VoidFunctionPtr)MaleWhale, (int)whale);
    t4->Fork((VoidFunctionPtr)FemaleWhale, (int)whale);
    t5->Fork((VoidFunctionPtr)MatchWhale, (int)whale);
}

void
WhaleTest3()
{
    Whale *whale = new Whale();
    Thread *t1 = new Thread("Thread 1");
    Thread *t2 = new Thread("Thread 2");
    Thread *t3 = new Thread("Thread 3");
    Thread *t4 = new Thread("Thread 4");
    Thread *t5 = new Thread("Thread 5");

    DEBUG ('w', "Lets mate some whales\n");

    t1->Fork((VoidFunctionPtr)MaleWhale, (int)whale);
    t4->Fork((VoidFunctionPtr)FemaleWhale, (int)whale);
    t2->Fork((VoidFunctionPtr)MaleWhale, (int)whale);
    t5->Fork((VoidFunctionPtr)MatchWhale, (int)whale);
    t3->Fork((VoidFunctionPtr)MaleWhale, (int)whale);

}

void
WhaleTest4()
{
    Whale *whale = new Whale();
    Thread *t1 = new Thread("Thread 1");
    Thread *t2 = new Thread("Thread 2");
    Thread *t3 = new Thread("Thread 3");
    Thread *t4 = new Thread("Thread 4");
    Thread *t5 = new Thread("Thread 5");
    Thread *t6 = new Thread("Thread 6");

    DEBUG ('w', "Lets mate some whales\n");

    t1->Fork((VoidFunctionPtr)MaleWhale, (int)whale);
    t4->Fork((VoidFunctionPtr)FemaleWhale, (int)whale);
    t5->Fork((VoidFunctionPtr)FemaleWhale, (int)whale);
    t2->Fork((VoidFunctionPtr)MaleWhale, (int)whale);
 
    t3->Fork((VoidFunctionPtr)MatchWhale, (int)whale);
    t6->Fork((VoidFunctionPtr)MatchWhale, (int)whale);

}

void
WhaleTest5()
{
    Whale *whale = new Whale();
    Thread *t1 = new Thread("Thread 1");
    Thread *t2 = new Thread("Thread 2");
    Thread *t3 = new Thread("Thread 3");
    Thread *t4 = new Thread("Thread 4");
    Thread *t5 = new Thread("Thread 5");
    Thread *t6 = new Thread("Thread 6");
    Thread *t7 = new Thread("Thread 7");
    Thread *t8 = new Thread("Thread 8");
    Thread *t9 = new Thread("Thread 9");

    DEBUG ('w', "Lets mate some whales\n");

    t3->Fork((VoidFunctionPtr)MatchWhale, (int)whale);
    t6->Fork((VoidFunctionPtr)MatchWhale, (int)whale);
    t1->Fork((VoidFunctionPtr)MaleWhale, (int)whale);
    t4->Fork((VoidFunctionPtr)FemaleWhale, (int)whale);
    t5->Fork((VoidFunctionPtr)FemaleWhale, (int)whale);
    t2->Fork((VoidFunctionPtr)MaleWhale, (int)whale);
    t7->Fork((VoidFunctionPtr)MaleWhale, (int)whale);
    t8->Fork((VoidFunctionPtr)FemaleWhale, (int)whale);
    t9->Fork((VoidFunctionPtr)MatchWhale, (int)whale);


}


void
Joiner(Thread *joinee)
{
  currentThread->Yield();
  currentThread->Yield();

  printf("Waiting for the Joinee to finish executing.\n");

  currentThread->Yield();
  currentThread->Yield();

  // Note that, in this program, the "joinee" has not finished
  // when the "joiner" calls Join().  You will also need to handle
  // and test for the case when the "joinee" _has_ finished when
  // the "joiner" calls Join().

  joinee->Join();

  currentThread->Yield();
  currentThread->Yield();

  printf("Joinee has finished executing, we can continue.\n");

  currentThread->Yield();
  currentThread->Yield();
}

void
Joinee()
{
  int i;

  for (i = 0; i < 5; i++) {
    printf("Smell the roses.\n");
    currentThread->Yield();
  }

  currentThread->Yield();
  printf("Done smelling the roses!\n");
  currentThread->Yield();
}

void
ForkerThread()
{
  Thread *joiner = new Thread("joiner", 0);  // will not be joined
  Thread *joinee = new Thread("joinee", 1);  // WILL be joined

  // fork off the two threads and let them do their business
  joiner->Fork((VoidFunctionPtr) Joiner, (int) joinee);
  joinee->Fork((VoidFunctionPtr) Joinee, 0);

  // this thread is done and can go on its merry way
  printf("Forked off the joiner and joiner threads.\n");
}

void
ChildThread(int which)
{
    int i = 0;
    for (i = 0; i < 20; i++) {
        printf("***Thread %d looped %d times\n", which, i);
    }
}

void
JoinTest1()
{
    Thread *t1 = new Thread("Child", 1);
    t1->Fork(ChildThread, 1);
    t1->Join();
    
}

void
JoinTest2()
{
    Thread *t1 = new Thread("Child", 1);
    t1->Fork(ChildThread, 1);
    t1->Join();
    t1->Join();
}

void
JoinOnlyCalledOnForkedThread()
{
    Thread *t1 = new Thread("Child", 1);
    t1->Join();
}

void
JoinWillNotJoinUnjoinableThread()
{
    Thread *t1 = new Thread("Child", 0);
    t1->Fork(ChildThread, 1);
    t1->Join();
    
    //should fail assert because t1 is unjoinable.
}

//----------------------------------------------------------------------
// ThreadTest
//  Invoke a test routine.
//----------------------------------------------------------------------

void
ThreadTest()
{
    switch (testnum) {
        case 1:
            ThreadTest1();
            break;
        case 2:
            LockTest1();
            break;
        case 3:
            PriorityTest1();
            break;
        case 4:
            PriorityTest2();
            break;
        case 5:
            PriorityTest3();
            break;
        case 6:
            PriorityInversionTest1();
            break;
        case 7:
            Test_release();
            break;
        case 8:
            Test_delete();
            break;
        case 9:
            MailboxTest();
            break;
        case 10:
            test_cond();
            break;
        case 11:
            WhaleTest1();
            break;
        case 12:
            WhaleTest2();
            break;
        case 13:
            WhaleTest3();
            break;
        case 14:
            WhaleTest4();
            break;
        case 15:
            WhaleTest5();
            break;
        case 16:
            JoinTest1(); //simple join test to make sure it works.
            break;
        case 17:
            ForkerThread(); //join example test.
            break;
        case 18:
            JoinTest2(); //make sure 2 joins can't happen on same thread.
            break;
        case 19:
            JoinOnlyCalledOnForkedThread();
            break;
        case 20:
            JoinWillNotJoinUnjoinableThread();
            break;
        case 22:
            test_cond2(0);
            break; 
        case 23:
            test_cond2(1);
            break;                       
        case 24:
            testcond3();
            break;
        case 25:
            PriorityTest4();
            break;
        case 26:
            PriorityInversionJoinTest1();
            break;
        default:
            printf("No test specified.\n");
            break;
    }
}
